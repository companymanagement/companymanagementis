using CompanyDataModels;
using CompanyEntities;
using DBGateway;
using Microsoft.EntityFrameworkCore;
using NLog.Extensions.Logging;

namespace CompanyDataAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();

            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .Build();

            var dbContext = (DbContextOptionsBuilder serviceProvider) =>
            {
                var connectionString = configuration["CompanyEntitiesDBConnectionString"];
                serviceProvider.UseSqlServer(connectionString);
            };

            builder.Services.AddDbContext<CompanyDBContext>(dbContext);

            builder.Services.AddAutoMapper(typeof(CompanyDataMapper));
            builder.Services.AddScoped<IGetEmployesBySalary, GetEmployesBySalary>();
            builder.Services.AddScoped<ICompanyDataOps, CompanyDataOps>();

            builder.Services.AddLogging(loggingBuilder =>
            {
                loggingBuilder.ClearProviders(); 
                loggingBuilder.SetMinimumLevel(LogLevel.Trace); 
                loggingBuilder.AddNLog("nlog.config"); 
            });

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
            );

            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}