using CompanyDataModels.Models;
using DBGateway;
using Microsoft.AspNetCore.Mvc;

namespace CompanyDataAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CompanyDataController : ControllerBase
    {
        private readonly ILogger<CompanyDataController> _logger;
        private readonly IGetEmployesBySalary _getBySalary;
        private readonly ICompanyDataOps _companyDataOps;

        public CompanyDataController(ILogger<CompanyDataController> logger, 
            IGetEmployesBySalary getBySalary, 
            ICompanyDataOps companyDataOps
        )
        {
            _logger = logger;
            _getBySalary = getBySalary;
            _companyDataOps = companyDataOps;
        }

        /// <summary>
        /// Saves company data to sql database, uses Id for duplicate check
        /// </summary>
        /// <param name="companyData"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveCompanyDataAsync")]
        public async Task<IActionResult> SaveCompanyDataAsync([FromBody] CompanyData companyData)
        {
            try
            {
                await _companyDataOps.SaveCompanyData(companyData);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return Ok();
        }

        /// <summary>
        /// Get Developers that has higher salary than their managers
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetHighestIncomeDevelopersAsync")]
        public async Task<JsonResult> GetHighestIncomeDevelopersAsync()
        {
            IEnumerable<CompanyEntities.Developer> devs = Enumerable.Empty<CompanyEntities.Developer>();
            try
            {
                devs = await _getBySalary.GetHighestPayedDevs();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }
            
            return new JsonResult(
                devs.Select(d => new 
                { 
                    d.FirstName, 
                    d.Salary 
                })
            );
        }

        /// <summary>
        /// Gets managers that manages some devs and no one of them has higher salary than manager itself
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetHighestIncomeManagersAsync")]
        public async Task<JsonResult> GetHighestIncomeManagersAsync()
        {
            IEnumerable<CompanyEntities.Manager> managers = Enumerable.Empty<CompanyEntities.Manager>();
            try
            {
                managers = await _getBySalary.GetHighestPayedManagers();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return new JsonResult(
                managers.Select(m => new
                {
                    m.FirstName,
                    m.LastName,
                    m.Salary
                })
            );
        }

    }
}