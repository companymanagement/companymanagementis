﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompanyDataModels.Models
{
    public class Employee
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Salary { get; set; }

        public string Email { get; set; }

        public Department Department { get; set; } = new Department();

        public int DepartmentId { get; set; }

        public List<WorkAddress> WorkAddresses { get; set; }

    }
}
