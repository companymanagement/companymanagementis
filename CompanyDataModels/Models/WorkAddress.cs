﻿namespace CompanyDataModels.Models
{
    public class WorkAddress
    {
        public int Id { get; set; }

        public string FullAddress { get; set; }

        public string WorkSpaceLocation { get; set; }

        public int WorkSpaceQualityLevel { get; set; }

        public int DeveloperId { get; set; }

        public int ManagerId { get; set; }
    }
}
