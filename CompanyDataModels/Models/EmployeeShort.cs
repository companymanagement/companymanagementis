﻿namespace CompanyManagementMVC.Models
{
    public class EmployeeShort
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Salary { get; set; }
    }
}
