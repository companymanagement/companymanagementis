﻿namespace CompanyDataModels.Models
{
    public class CompanyData
    {
        public List<Department> Departments { get; set; }

        public List<Developer> Developers { get; set; }

        public List<Manager> Managers { get; set; }

        public List<WorkAddress> WorkAddress { get; set; }
    }
}
