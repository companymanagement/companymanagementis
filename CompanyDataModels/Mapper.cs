﻿using AutoMapper;
using CompanyEntities;
using CompanyManagementMVC.Models;

namespace CompanyDataModels
{
    public class CompanyDataMapper : Profile
    {
        public CompanyDataMapper()
        {
            CreateMap<Models.Department, Department>()
                .ReverseMap();   

            CreateMap<Models.Developer, Developer>()
                .ReverseMap();

            CreateMap<Developer, EmployeeShort>()
                .ReverseMap();

            CreateMap<Models.Manager, Manager>()
                .ReverseMap();

            CreateMap<Manager, EmployeeShort>()
                .ReverseMap();

            CreateMap<Models.WorkAddress, WorkAddress>()
                .ReverseMap();
        }

    }
}
