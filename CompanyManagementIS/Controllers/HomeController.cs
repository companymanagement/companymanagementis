﻿using CompanyManagementIS.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using CompanyManagementMVC.Models;
using DBGateway;
using AutoMapper;

namespace CompanyManagementIS.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMapper _mapper;
        private readonly IGetEmployesBySalary _getBySalary;

        public HomeController(ILogger<HomeController> logger, 
            IMapper mapper, 
            IGetEmployesBySalary getBySalary)
        {
            _logger = logger;
            _mapper = mapper;
            _getBySalary = getBySalary;
        }

        public async Task<IActionResult> Devs()
        {
            var viewModel = new EmployeesModel()
            {
                IsManager = false
            };

            try
            {
                var devs = await _getBySalary.GetHighestPayedDevs();

                viewModel.Employees = devs.Select(_mapper.Map<EmployeeShort>).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return View("EmployeTable", viewModel);
        }

        public async Task<IActionResult> Managers()
        {
            var viewModel = new EmployeesModel();
            try
            {
                var managers = await _getBySalary.GetHighestPayedManagers();

                viewModel.Employees = managers.Select(_mapper.Map<EmployeeShort>).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
            }

            return View("EmployeTable", viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}