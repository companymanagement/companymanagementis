﻿namespace CompanyManagementMVC.Models
{
    public class EmployeesModel
    {
        public List<EmployeeShort> Employees { get; set; }

        public bool IsManager { get; set; } = true;
    }
}
