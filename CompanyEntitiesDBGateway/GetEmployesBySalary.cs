﻿using CompanyEntities;

namespace DBGateway
{
    public class GetEmployesBySalary : IGetEmployesBySalary
    {
        private readonly CompanyDBContext _dbContext;

        public GetEmployesBySalary(CompanyDBContext dbcontext) 
        {
            _dbContext = dbcontext;
        }

        public async Task<IEnumerable<Developer>> GetHighestPayedDevs() =>
            await Task.Run(() => _dbContext.Developer
                    .Where(d => _dbContext.Manager
                        .Any(m => m.Id == d.ManagerId && d.Salary > m.Salary)));

        public async Task<IEnumerable<Manager>> GetHighestPayedManagers() =>
            await Task.Run(() => _dbContext.Manager
                    .Where(m => m.Developers.Count() > 0 &&
                               !m.Developers.All(d => d.Salary > m.Salary)));
    }
}
