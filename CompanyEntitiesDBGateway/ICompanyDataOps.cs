﻿using CompanyDataModels.Models;

namespace DBGateway
{
    public interface ICompanyDataOps
    {
        Task SaveCompanyData(CompanyData companyData);
    }
}
