﻿namespace DBGateway
{
    public interface IGetEmployesBySalary
    {
        Task<IEnumerable<CompanyEntities.Manager>> GetHighestPayedManagers();

        Task<IEnumerable<CompanyEntities.Developer>> GetHighestPayedDevs();
    }
}
