﻿using AutoMapper;
using CompanyDataModels.Models;
using CompanyEntities;

namespace DBGateway
{
    public class CompanyDataOps : ICompanyDataOps
    {
        private readonly CompanyDBContext _dbContext;
        private readonly IMapper _mapper;

        public CompanyDataOps(CompanyDBContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        private CompanyEntities.Department GetDepartmentById(int id) => 
            _dbContext.Department?.FirstOrDefault(d => d.Id == id) ?? new CompanyEntities.Department();

        public async Task SaveCompanyData(CompanyData companyData)
        {
            var dbDepartment = companyData.Departments
                .Select(_mapper.Map<CompanyEntities.Department>)
                .Where(d => !_dbContext.Department.Any(dpm => dpm.Id == d.Id));

            _dbContext.Department.AddRange(dbDepartment);

            await _dbContext.SaveChangesAsync();

            var dbManagers = companyData.Managers
                .Select(m =>
                {
                    var manager = _mapper.Map<CompanyEntities.Manager>(m);
                    manager.Department = GetDepartmentById(m.Id);

                    return manager;
                })
                .Where(m => !_dbContext.Manager.Any(dbm => dbm.Id == m.Id));

            _dbContext.Manager.AddRange(dbManagers);

            var dbDevelopers = companyData.Developers
                .Select(d =>
                {
                    var developer = _mapper.Map<CompanyEntities.Developer>(d);
                    developer.Department = GetDepartmentById(d.Id);

                    return developer;
                })
                .Where(m => !_dbContext.Developer.Any(dbd => dbd.Id == m.Id));

            _dbContext.Developer.AddRange(dbDevelopers);

            await _dbContext.SaveChangesAsync();

            var dbWorkAddress = companyData.WorkAddress
                .Select(_mapper.Map<CompanyEntities.WorkAddress>)
                .Where(m => !_dbContext.WorkAddress.Any(dbw => dbw.Id == m.Id));

            _dbContext.WorkAddress.AddRange(dbWorkAddress);

            await _dbContext.SaveChangesAsync();
        }
    }
}
