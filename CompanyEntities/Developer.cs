﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CompanyEntities
{
    public class Developer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [MaxLength(300)]
        public string FirstName { get; set; }

        [MaxLength(300)]
        public string LastName { get; set; }

        [Required]
        public int Salary { get; set; }

        [MaxLength(500)]
        public string Email { get; set; }

        [Required]
        public int ManagerId { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public ICollection<WorkAddress> WorkAddresses { get; set; }
    }
}

