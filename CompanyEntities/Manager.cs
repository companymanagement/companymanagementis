﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyEntities
{
    public class Manager
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [MaxLength(300)]
        public string FirstName { get; set; }

        [MaxLength(300)]
        public string LastName { get; set; }

        public int Salary { get; set; }

        [MaxLength(500)]
        public string Email { get; set; }

        [ForeignKey("DepartmentId")]
        public Department Department { get; set; }

        public ICollection<WorkAddress> WorkAddresses { get; set; } = new List<WorkAddress>();

        public ICollection<Developer> Developers { get; set; }
    }
}
