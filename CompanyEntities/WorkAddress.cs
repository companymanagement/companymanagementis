﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace CompanyEntities
{
    public class WorkAddress
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [MaxLength(1000)]
        public string FullAddress { get; set; }

        [MaxLength(500)]
        public string WorkSpaceLocation { get; set; }

        [Range(0, 10)]
        public int WorkSpaceQualityLevel { get; set; }

        public int DeveloperId { get; set; }

        public int ManagerId { get; set; }
    }
}
