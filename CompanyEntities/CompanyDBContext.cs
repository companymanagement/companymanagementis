﻿using Microsoft.EntityFrameworkCore;

namespace CompanyEntities
{
    public class CompanyDBContext : DbContext
    {
        public CompanyDBContext(DbContextOptions options) : base (options) { }

        public DbSet<Department> Department { get; set; }
        public DbSet<Developer> Developer { get; set; }
        public DbSet<Manager> Manager { get; set; }
        public DbSet<WorkAddress> WorkAddress { get; set; }
    }
}
